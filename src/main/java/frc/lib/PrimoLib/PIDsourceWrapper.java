
package frc.lib.PrimoLib;

import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;

/**
 * Add your docs here.
 */
public abstract class PIDsourceWrapper implements PIDSource{
    @Override
    public PIDSourceType getPIDSourceType() {
        return PIDSourceType.kDisplacement;
    }
    @Override
    public void setPIDSourceType(PIDSourceType pidSource) {
        setPIDSourceType(PIDSourceType.kDisplacement);
    }
    @Override
    abstract public double pidGet();

}
