package frc.robot;

public class RobotMap {
  // Driver Motors:
  public final static class DriverPorts {
    public static final int driverFrontRight = 0;
    public static final int driverFrontLeft = 1;
    public static final int driverFollowerRight = 0;
    public static final int driverFollowerLeft = 1;
    
  }
  
  public final static class ElevatorPorts {
    public static final int elevatorTalonL = 2;
    public static final int elevatorTalonR = 3;
    public static final int elevatorFollowerVictorL = 2;
    public static final int elevatorFollowerVictorR = 3;
    public static final int elevatorMag = 1;
    public static final int elevatorMicFloor = 9;
    public static final int compressor = 0;
  }

  public final static class GripperPorts{
    public static final int moveGripper = 1;
    public static final int rollerGripper = 0;
    public static final int opticalEncoder = 2;
    public static final int pushPanel = 3;
    public static final int vacSol = 2;
    public static final int gripperSwitch = 3;
    public static final int opticalDigital = 0;
  }
}
