package frc.robot.Utils;

import edu.wpi.first.wpilibj.Notifier;

public class Constants {

    public static class ElevatorConstants{
        public static final int time_out = 30; // in millisecs
         // Feeder positions
        public static final int FEEDER_CARGO = 0;
        public static final int FEEDER_PANEL = 0;
    
        //closed
        public static final int CLOSED = 0;
    
        //cargo ship
        public static final int CARGOSHIP_CARGO = 0;
        public static final int CARGOSHIP_PANEL = 0;
    
        //rocket positions
        //hatches
        public static final int ROCKET_HIGH_HATCH = 0;
        public static final int ROCKET_MID_HATCH = 0;
        public static final int ROCKET_LOW_HATCH = 0;
        //cargo
        public static final int ROCKET_LOW_CARGO = 0;
        public static final int ROCKET_MID_CARGO = 0;
        public static final int ROCKET_HIGH_CARGO = 0;
    }

}
