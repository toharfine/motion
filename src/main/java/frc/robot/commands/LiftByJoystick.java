package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;
import frc.robot.OI;
import frc.robot.subsystems.Elevator;

public class LiftByJoystick extends Command {

  private Elevator elevator;
  private boolean climbStart;
  private double elevatorSpeed;
  private OI oi;
  private boolean flag;
  private Timer time;
  private boolean switchChanged;

  public LiftByJoystick() {
    this.elevator = Robot.elevator;
    this.oi = Robot.m_oi;
    flag = false;
    time = new Timer();
    switchChanged = true;
  }

  @Override
  protected void initialize() {
    requires(this.elevator);
    climbStart = false;
  }

  @Override
  protected void execute() {
    this.elevatorSpeed = (this.oi.joystickOperator.getRawAxis(1) * SmartDashboard.getNumber("MaxElevatorSpeed", 0.9));
    if (!Robot.commandFlag && Math.abs(this.elevatorSpeed) < 0.1) {
      if (flag == false) {
        elevator.setElevatorSpeed(0.056);
        flag = true;
      }
    } else if (!Robot.commandFlag) {
      this.elevator.setElevatorSpeed(elevatorSpeed);
      flag = false;
    }

    //climb sequence
    if (Robot.climbingFlag && !climbStart) 
    {
      time.start();
      climbStart = true;
    }
    if (climbStart && time.get() < 1)
    {
      this.elevator.setElevatorSpeed(0.2);
    }
    else if(climbStart)
    {
      this.elevator.setElevatorSpeed(elevatorSpeed);
    
    }
    }


    
   

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    this.elevator.setElevatorSpeed(0.0);
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
    this.elevator.setElevatorSpeed(0.0);
  }
}
