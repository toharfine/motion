package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.robot.RobotMap;

public class Gripper extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  public static enum GripperState {
    OpenLoop, PositionControl, Disabled, Climb
  }
  // public static enum GripperPositions{
  // Closed,
  // Mid,
  // Open;
  // }

  private Spark moveGripper;
  private Spark rollerGripper;
  private Counter opticalEncoder;
  private DigitalInput gripperSwitch;
  private Solenoid panelArm;
  private Solenoid vacSol;
  // public static GripperPositions gripperPosition;
  private static Gripper instance = null;

  // constructor function
  public Gripper() {
    this.moveGripper = new Spark(RobotMap.GripperPorts.moveGripper);
    this.rollerGripper = new Spark(RobotMap.GripperPorts.rollerGripper);
    ;
    this.opticalEncoder = new Counter(RobotMap.GripperPorts.opticalEncoder);
    this.gripperSwitch = new DigitalInput(RobotMap.GripperPorts.gripperSwitch);
    this.panelArm = new Solenoid(RobotMap.GripperPorts.pushPanel);
    this.vacSol = new Solenoid(RobotMap.GripperPorts.vacSol);
    // gripperPosition = GripperPositions.Closed; //The Gripper starts inside the
    // robot.

  }

  public static Gripper getInstance() {
    if (instance == null) {
      instance = new Gripper();
    }
    return instance;
  }

  // get OpticalEncoder Value
  public double getOpticalEncoderDistance() {
    return this.opticalEncoder.getDistance();
  }

  // reset OpticalEncoder
  public void resetOpticalEncoder() {
    this.opticalEncoder.reset();
  }

  // gets the speed of the right and left gripper motors (spark)
  public double getGripperSpeed() {
    return this.moveGripper.get();
    // return this.moveGripperLeft.get();
  }

  // sets the speed of the right and left gripper motors (spark)
  public void setGripperSpeed(double speed) {
    this.moveGripper.set(speed);
  }

  // gets the speed of the roller gripper speed
  public double getRollerGripperSpeed() {
    return this.rollerGripper.get();
  }

  // sets the speed of the roller gripper's speed
  public void setRollerGripperSpeed(double speed) {
    this.rollerGripper.set(speed);
  }

  public boolean getVacSol() {
    return this.vacSol.get();
  }

  public boolean getGripperSwitch() {
    return this.gripperSwitch.get();
  }

  // switch the vacRight solenoid and vacLeft solenoid state
  public void switchVac() {
    System.out.println("Switch Vac");
    this.vacSol.set(!(getVacSol()));
  }

  // gets if the
  public boolean getPanelArm() {
    return this.panelArm.get();
    // return this.pushPanelRight.get();
  }

  // option one to set the solenoid
  public void switchPanelArm() {
    this.panelArm.set(!(getPanelArm()));
  }

  // option two to set the solenoid
  public void setPanelPush(boolean isPushed) {
    this.panelArm.set(isPushed);
  }

  /////////////////////////////////////////////
  // stop motors
  public void stopGripperMotor() {
    this.moveGripper.set(0);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
